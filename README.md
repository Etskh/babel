# babel

## Introduction

Babel is (will be) an API/specification builder, exporter, and transcompiler manager.
Code may be outlined and at least partially written, then exported as a package.
This package may be imported into other projects to use, or have it all transcompiled/
exported to a different language to integrate it.

This way, code can be designed (sometimes implemented), and exported to any
existing codebase.

Babel (working title), as there is already a flashy, Javascript, programmy thing
with the name Babel, despite it seemingly only going from one language to
another.


# Roadmap

 * Creating a new package
 * Modifying a package
 * Running an Entry package
   * Any static executable area is a possible entry
 * Saving a package
    * JSON format package
    * then JSON that is gzipped
 * Loading a package
 * Exporters
    * C++ static library
    * npm package
    * Python module
 * Importer


## Installation

### Prerequisites

 * CMake installed locally. Just use the system's package manager... or if you're
 still on a darkages computer, download it from its site.
 * Awesomium - it'll mostly detect it automatically. You need to download it
 from its site.
 * SDL2 - it'll mostly detect it automatically. You'll also need to download it
 from its site.
 * Duktape - it's already in here, but if it isn't, throw it into /extern

### Process

 1. Download this repo.
 2. `cd` into `build`
 3. Run `cmake ..`
 4. Make sure everything's hunky-dorey
 5. Run `make`
 6. To test it, run `Babel` from the pwd
    *NIX: `./Babel`
    Windows: `Babel.exe`

# Background

## Conception

After writing a pretty robust Console API framework in Python for displaying
coloured output, logging, hiding repeated entries, and other fun things, I needed
to work on a CLI for a PHP project... but now I lacked a tool into which I poured
into a great deal of effort.

I contemplated porting the code from Python to PHP, but with the regular
expressions, and pythonic conventions would need to change. Anonymous functions
don't have very good support in PHP, and won't work if I'm building a Phar.

Why, in the 21st century, do we as an opensource community have so many projects
that do the same thing, with ports to different languages that are interpretted
differently, and have most of the same features. Python has anonymous functions,
so I guess PHP will get anonymous functions in the next release. C++ has lambda
functions, for crying out loud.

To improve code-reuse on a large scale, Babel(working title) attempts to tie
the gap between all the code, by not creating a new Be-All-End-All language
with X's features and Y's syntax... but create a specification builder, exporter,
and transcompiler manager.

This way, code can be designed (sometimes implemented), and exported to any
existing codebase.


# Code Conventions

## Semantic Function Naming
For any function that returns a value (save Error objects), the function must
unambiguously sound like it would return a type of value.

Here are a list of examples for prefixes and suffixes that are reasonable for
functions so that their names imply what's being returned.

### Semantics

	Booleans: has*, is*
	Integers: *count, *size, *length
	Obects: get*

### Examples

	isDone() instead of done()
	wheelCount() instead of wheels()
	

## Accessors and Manipulators

Writing simple accessors and manipulators is discouraged.

If at some later date, it might be important to put logic in the accessor, and all the while,
it was a public variable, it might seem reasonable to make a vanilla accessor. However, 
with the advances in automatic refactoring tools (find and replace), it wouldn't be hard
to just cut the cruft out now, and add in cruft in later.

Accessors don't always imply what they're returning - see Semantic Function Naming.
It adds to code bloat and if I'm able to freely get and set them at will, then it doesn't
matter to the class if I'm doing whichever operations in between.

Member functions should help in the understanding of what to do with them. Consider
the class:

```:c++
	class Homeroom {
		// ... other functions

	public:
		int students() { return students_;}
		void students(int studentNum) { students_(studentNum); }

	private:
		int students_;
	}
```

A common operation might be to add a student to the class, and another operation would
be to take a student out of the class. The driver code, given this class, would look like this,
given we don't have temporary variables:

```:c++
	Homeroom home;
	home.students( home.students() + 1 );
	printf("There are %d student(s) in the class.\n", home.students() );
```

The meaning of the students function is muddy. The code looks redundant.

It would make more sense, since we can already modify the member variable students_,
albeit indirectly, to just make the member public.

```:c++
	class Homeroom {
		// ... other functions
	public:
		int students;
	}
```

This makes the driver code much more readable, but still a little ambiguous about
what the type of value is stored in `students`

```:c++
	Homeroom home;
	++home.students;
	printf("There are %d student(s) in the class.\n", home.students );
```

But a much better way to do the class altogether would be to have a semantic accessor
and an addition method:

```:c++
	class Homeroom {
		// ... other functions
	public:
		int studentCount() { return student_count_;
		void addStudent() { ++ student_count_; }

	private:
		int student_count_;
	}
```

Now the driver code reads as exactly what its doing.

```:c++
	Homeroom home;
	home.addStudent();
	printf("There are %d student(s) in the class.\n", home.studentCount() );
```

Since this is the kind of operation we expect from this object, it would be extremely easy
to refactor it to take, for example, student data when it goes to add it.


### Subroutines

Functions that return void, alter the state, and therefore are known as
subroutines. It is just a different way to break down the code. If this
subroutine is only called in one place, then consider just putting it
into its own collabsible block in the function its called in.

### Member Variables

Each member of a class will be denoted by _lowerCase;
In the exported framework, all cases can be exported to a different case convention
because we have the technology.

### Mandatory Commments

After each and every namespace block, the terminating brace must have ` // namespace <name>`
on the same line.

Every class, after its terminating brace and semicolon, must have a comment ` // class <name>`
This helps readability, especially in the realm of inline functions (defined after the class definition).